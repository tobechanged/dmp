/**
 * Created by gregor on 11.06.16.
 */
var HASH = '1371f69f108d7dc6a1597e419b4cf202';
var CHUNKSIZE = 50000;
var lowASCII = 96;
var arrayLen = 5;
var finished = false;
var cleartext = '';
var lastarray = [];
for (var i = 0; i < arrayLen; i++) {
  lastarray.push(lowASCII);
}
console.log(lastarray);

module.exports.generateJob = function () {
  // return jobObject
  if (finished) {
    return null;
  } else {
    var temparray = lastarray;
    var count = 0;
    for (var i = 0; i <= CHUNKSIZE; i++) {
      lastarray = increase(lastarray, 0);
    }
    return [temparray, CHUNKSIZE, HASH];
  }
}

module.exports.onResult = function (finishedJob) {
  // finishedJob[true, cleartext]
  //  or
  // finishedJob[false, 0]
  console.log(finishedJob);
  if (finishedJob[0]) {
    finished = true;
    cleartext = finishedJob[1];
  } else {
    console.log('Leider nein');
  }
};

module.exports.onAllResults = function () {
  console.log('TADDAAAAAAAA: ' + cleartext);
};

function increase(a, i) {
  if (a[i] == 123) {
    a[i] = lowASCII;
    increase(a, i + 1);
  } else {
    a[i]++;
  }
  return a;
}
