var counter = 0;

module.exports.generateJob = function () {
  counter += 1;
  return (counter <= 1000) ? counter : null;
};

module.exports.onResult = function(finishedJob) {
  // console.log(finishedJob);
};

module.exports.onAllResults = function() {
  console.log("Job ist fertig und wurde erfolgreich abgeschlossen!");
};