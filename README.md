# distributed.js
This project is about using unused computing power of everybody's 
computers to solve computing intense problems. Our architecture 
scales horizontally over many web browsers. It can be embedded into 
every website. Problems to be computed are stored in simple unified 
modules, which are easily interchangable.

# Run
to run the code, simply clone the repo and run launch.sh
