/**
 * Created by jaro on 10.06.16.
 * this file is the one, that should be included into the web page...
 * this should be the ONLY thing you have to do
 * <script type="application/javascript" language="JavaScript" src="distributed.js"></script>
 */

function start() {
    console.log("started!");

    //estimate the script URL
    var baseUrl = "";
    var divs = document.getElementsByTagName("script");
    for (var i = 0; i < divs.length; i++) {
        if (divs[i].getAttribute("src").indexOf("distributed.js") > -1) {
            var url = divs[i].getAttribute("src");
            baseUrl = url.substr(0, url.length - "distributed.js".length);
            break;
        }
    }

    var url = baseUrl + "worker/index.html";
    console.log(url);
    updateThreads(url);
    setInterval(function () {
        updateThreads(url);
    }, 1000);
}

function updateThreads(url) {
    //estimate the cores depending on hardware and battery status
    var threads = navigator.hardwareConcurrency - 1;
    if (isNaN(threads)) {
        threads = 3;
    }
    try {
        navigator.getBattery().then(function (battery) {
            if (battery.level < .3 && !battery.charging) {
                threads = 1;
            }
            updateIFrames(url, threads);
        });
    } catch (ignore) {
        threads = 2;
        updateIFrames(url, threads);
    }
}

function updateIFrames(url, threads) { //make array of existing iframes
    var ourFrames = [];
    var iframes = document.getElementsByTagName("iframe");
    for (var i = 0; i < iframes.length; i++) {
        if(iframes[i].getAttribute("class")) {
            var indexof = iframes[i].getAttribute("class").indexOf("thread");
            if (indexof > -1) {
                ourFrames.push(iframes[i]);
            }
        }
    }

    if (ourFrames.length < threads) { //add iframes
        console.log("adding " + (threads - ourFrames.length) + " threads");
        for (i = 0; i < (threads - ourFrames.length); i++) {
            document.getElementById('worker').innerHTML += "<div class='col s6 m4'><iframe class='thread' src='" + url + "' style='border: 1px white solid; min-height: 150px;'></iframe></div>";
        }
    } else if (ourFrames.length > threads) { //remove iframes
        console.log("removing " + (ourFrames.length - threads) + " threads");
        for (i = 0; i < ourFrames.length - threads; i++) {
            ourFrames[i].parentNode.removeChild(ourFrames[i]);
        }
    }

}

window.onload = start;
