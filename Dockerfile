FROM debian:jessie
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y nodejs npm git
WORKDIR /opt
RUN git clone https://gitlab.com/tobechanged/dmp.git
WORKDIR dmp/backend
RUN npm i
EXPOSE 1337
ENTRYPOINT /bin/bash
